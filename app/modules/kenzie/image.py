import os
from flask.helpers import safe_join
from werkzeug.datastructures import FileStorage

from werkzeug.utils import secure_filename

from . import FILES_DIRECTORY
from . import ALLOWED_EXTENSIONS


def list_all():
    images_list = []
    for path, _, files in os.walk(FILES_DIRECTORY):
        for name in files:
            images_list.append(os.path.join(path, name))
    return images_list


def list_by_extension(extension: str):
    images_list = []
    for path, _, files in os.walk(FILES_DIRECTORY):
        if path == f'{FILES_DIRECTORY}/{extension}':
            for name in files:
                images_list.append(os.path.join(path, name))
    return images_list


def save_image(file: FileStorage):
    file_extension = file.filename.split('.')[-1]
    if file_extension in ALLOWED_EXTENSIONS:
        if not os.path.isdir(f'{FILES_DIRECTORY}/{file_extension}'):
            os.mkdir(f'{FILES_DIRECTORY}/{file_extension}')
        path = safe_join(f'{FILES_DIRECTORY}/{file_extension}', file.filename)
        if not os.path.isfile(path):
            file.save(path)
        else:
            return 'File alread exists', 409

        return file.filename, 201
    else:
        return 'Unsupported format', 415


def get_path(filename: str):
    file_extension = filename.split('.')[-1]
    path = safe_join(f'{FILES_DIRECTORY}/{file_extension}', filename)
    return path


def download_zip_path(file_extension: str, compression_ratio: int):
    if file_extension == None:
        path = '/tmp/images.zip'
        comando = f'zip -{compression_ratio} -r {path} {FILES_DIRECTORY}'
    else:
        path = f'/tmp/images_{file_extension}.zip'
        folder_path = f'{FILES_DIRECTORY}/{file_extension}'
        comando = f'zip -{compression_ratio} -r {path} {folder_path}'
    os.system(comando)
    return path
