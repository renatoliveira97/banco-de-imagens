import os
from environs import Env
from os import environ

env = Env()
env.read_env()

FILES_DIRECTORY = environ.get('FILES_DIRECTORY')
MAX_CONTENT_LENGTH = environ.get('MAX_CONTENT_LENGTH')
ALLOWED_EXTENSIONS = environ.get('ALLOWED_EXTENSIONS')

if not os.path.isdir(FILES_DIRECTORY):
    os.mkdir(FILES_DIRECTORY)
    