import os
from flask import Flask, jsonify, send_file, request
from .modules.kenzie import FILES_DIRECTORY, image

app = Flask(__name__)
app.config['MAX_CONTENT_LENGTH'] = 1024 * 1024


@app.route('/files')
def list_all_images():
    images_list = image.list_all()
    return jsonify(images_list), 200


@app.route('/files/<string:extension>')
def list_images_by_extension(extension: str):
    images_list = image.list_by_extension(extension)
    return jsonify(images_list), 200


@app.post('/upload')
def upload_image():
    for file in request.files:
        filename = image.save_image(request.files[file])
        
    return filename[0], filename[1]


@app.route('/download/<string:file_name>')
def download_image(file_name: str):
    path = image.get_path(file_name)
    print(os.path.abspath(path))
    if os.path.isfile(path):
        return send_file(f'.{path}', as_attachment=True), 200
    else:
        return 'File does not exist', 404


@app.route('/download-zip')
def download_dir_as_zip():
    if len(os.listdir(FILES_DIRECTORY)) != 0:
        file_extension = request.args.get('file_extension', None, str)
        compression_ratio = request.args.get('compression_ratio', 6, int)
        path = image.download_zip_path(file_extension, compression_ratio)
        return send_file(path, as_attachment=True), 200
    else: 
        return 'Empty directory', 404


@app.errorhandler(413)
def request_entity_too_large(error):
    return 'File Too Large', 413
